[Repo URL]: https://gitlab.csi.miamioh.edu/meyerjm/miami-university-cse-601-group-project.git
[GitHub Desktop]: https://desktop.github.com/

# CSE 601: HotDog Group Project

## How to use Git

### GUI (Recommended)

Set up:

1. Download & install [GitHub Desktop]

1. GitHub Desktop will ask you to sign in to a GitHub account.
  This is optional and will not matter either way since we are using GitLab instead of GitHub.com.

1. GitHub Desktop may ask you for your name and email.
  Your email will be visible to any project you contribute code to.

1. Click on "New >> Clone Repository"

1. Type in this [URL][Repo URL]

1. Open up your favorite shell (e.g. Powershell, Git Bash, etc.)
  and go to `~/Documents/GitHub/[REPO_NAME]`

Usage/Workflow:

1. Pull changes

1. Make changes

1. Commit changes using a meaningful summary message
  (written in the imperative tense)

1. Push changes

### Commandline

Set up:

```bash
cd ~/Documents/

git clone https://gitlab.csi.miamioh.edu/meyerjm/miami-university-cse-601-group-project.git
cd miami-university-cse-601-group-project

git config core.autoCrlf input
```

Usage/Workflow:

```bash
# Pull changes
git pull

# Make changes
# ...

# Add changes to staging area
git add . -p

# Commit changes using a meaningful summary message (written in the imperative tense)
git commit -m "Do the thing"

# Push changes
git push
```

## Resources

### R

> [Style guide](https://style.tidyverse.org/)
>
> Tidyverse cheatsheets:
>
>   - [R](https://github.com/rstudio/cheatsheets/blob/master/base-r.pdf)
>   - [readr/tidyr](https://github.com/rstudio/cheatsheets/blob/master/data-import.pdf)
>   - [dplyr](https://github.com/rstudio/cheatsheets/blob/master/data-transformation.pdf)
>   - [lubridate](https://github.com/rstudio/cheatsheets/blob/master/lubridate.pdf)
>   - [ggplot2](https://github.com/rstudio/cheatsheets/blob/master/data-visualization-2.1.pdf)
>   - [stringr](https://github.com/rstudio/cheatsheets/blob/master/strings.pdf)
>   - [purrr](https://github.com/rstudio/cheatsheets/blob/master/purrr.pdf)
>
> Tidyverse resources:
>
>   - https://moderndive.com/
>   - https://r4ds.had.co.nz/
>   - https://www.tidyverse.org/learn/

Installing a library:

```R
install.packages("libraryname")
```

### Python

> [Python guide](https://docs.python-guide.org/)

Installing a library:

```bash
pipenv install libraryname
```
